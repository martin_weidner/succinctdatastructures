\documentclass[11pt, bibtotoc]{article}
\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage{hyperref}
\usepackage[square]{natbib}
\usepackage{a4}
\usepackage{fullpage}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{graphicx} % include graphics with \includegraphics{xyz.pdf}
%\usepackage[algoruled]{algorithm2e} % \begin{algorithm} ... \end{algorithm}
\usepackage[lined,boxed]{algorithm2e}
\newtheorem{thm}{Theorem}
\newtheorem{lem}[thm]{Lemma}
\newtheorem{cor}[thm]{Corollary}
\newtheorem{prop}[thm]{Proposition}
\newtheorem{defi}{Definition}
\newtheorem{obs}{Observation}
\newtheorem{prob}{Problem}
\newtheorem{ex}{Example}


\DeclareMathOperator*{\argmax}{argmax}
\DeclareMathOperator*{\argmin}{argmin}

\pagestyle{plain}

\author{Schriftführer: Martin Weidner}
\title{Fortgeschrittene Datenstrukturen --- Vorlesung 11}
\date{19.01.2012}

\begin{document}
\maketitle

\section{Succinct Data Structures (ctd.)}
\subsection{Select-Queries}
A slightly different approach, compared to rank, is used for select. $B$ represents the bit-vector with $|B| = n$ and let $k = \lfloor\log^2 n\rfloor$. A new table $N$ is defined, which stores the $(k\cdot i)$'th occurence of a 1-bit in $B$. Alternatively, $N[i] = \operatorname{select}_{1}(B, ik)$. As we can see, table $N[1, \frac{n}{k}]$ divides $B$ into \emph{blocks} of different sizes, whereas each \emph{block} contains $k$ 1's. An example is given in Figure~\ref{fig:11:sub_div}, where an abstract bit-vector $B$ is divided into \emph{blocks} with $k$ 1's in each. 
\begin{ex}
	Let $N = [17, 28, 36, 53, ...]$ and $k=8$. In this case, the 8\textsuperscript{th} 1 would be at index $17$ in $B$, the 16\textsuperscript{th} 1 at index $28$ and so on.
\end{ex}
	\begin{figure}[!ht]     
		\centering  
		\includegraphics[width=0.9\textwidth]{images/subrange_division.pdf}
		\caption{Division of $B$ into blocks with $k$ 1's}  
		\label{fig:11:sub_div}
	\end{figure}
\noindent The resulting \emph{blocks} are grouped as follows:
\begin{defi}
	A \emph{long block} spans more than $k^2 = \Theta(\log^4 n)$ positions in $B$.
\end{defi}\noindent
	Its number is limited by $\frac{n}{\log^4 n}$. Therefore, the answers for select-queries within all long blocks can be stored explicitly in a table: $\operatorname{LongBlock}[0, \frac{n}{\log^4 n}][1, k]$, where $\operatorname{LongBlock}[i][j] = \operatorname{select}_1(B, ik+j)$. Moreover, the LongBlock table is indexed by \emph{potential} block numbers, because we do not know how many long blocks there are before a given position. Therefore, we imagine that a long block begins at every $k^2$ position. Select-queries to long blocks can be responded completely based on this structure.

\begin{defi}
	A \emph{short block} spans $\le k^2$ positions in $B$.
\end{defi}\noindent
 It contains $k$ 1-bits and it spans $\le k^2$ positions in $B$ at most. We divide their range of arguments into sub-ranges of: $k' = \lfloor \log^2 k \rfloor = \Theta (\log^2\log n) $. Then, a table $N'[1,\frac{n}{k'}]$ is defined, whereas the answers to select-queries for multiples of $k'$ (relative to the end of the previous block) are stored. In table $N'$, a $\bot$ symbol indicates if we are in a long block. The formal definition of $N'$ is: $N'[i] = \operatorname{select}_1 (B, ik') - (N[\frac{ik'}{k}]) $, with $\frac{ik'}{k}$ as the block before i\textsuperscript{th} $1$ and the subtrahend, representing the end of the block.

\noindent Table $N'$ divides the blocks into \emph{miniblocks}, each containing $k'$ 1-bits. A miniblock is called \emph{long} if it spans more than $s = \frac{\sqrt{k}}{2} = \frac{\log n}{2}$ positions in $B$, and \emph{short} otherwise. Analogous to the long blocks, the answers to all select-queries are stored explicitly for all \emph{long miniblocks}, relative to the beginning of the corresponding short block. The table $\operatorname{LongMiniBlock}[0, \frac{n}{s}][1, k']$ is indexed by the \emph{potential} long miniblock numbers, because the number of long miniblocks up to a given position is unknown.\\
Finally, a \emph{lookup table} is stored for the \emph{short miniblocks} because of its relative small size.
\begin{defi}[Lookup table for small miniblocks] The lookup table for short miniblocks is defined as follows:
	$\operatorname{Inblock}[0, 2^s-1][1, k']$, where: $\operatorname{Inblock}[\text{pattern}][i] = \operatorname{select}_1(\text{pattern}, i)$ for all bit-patterns of length $s$ and $\forall 1 \le i \le k'$.
\end{defi}
\noindent Based on this table, a select-query within a \emph{short miniblock} $B[b,e]$ can be answered by looking at $\operatorname{Inblock}[B[b, b+s-1]][i]$. We should keep in mind that \emph{short miniblock} could be shorter than $s$. In this case, a padding with arbitrary bits in the end to match exactly $s$ bits does not affect the select query answer. \bigskip\\
The \emph{query procedure} follows the description of the data structure. Note that we can determine if (mini-) blocks are long or short by inspecting adjacent elements of $N$ (or $N'$) and checking if they differ by more than $k^2$ (or $\frac{\sqrt{k}}{2}$).
\bigskip\\
	In the following, it is verified that the required bit space of the defined structures for the select query are succinct.
	\begin{description}
		\item[Table $N$] The $N$ table can have $\frac{n}{k}$ entries as maximum in the case that $B$ only contains 1's. Moreover, one stored index requires $\le \log n$ bits. We get: $|N| = \frac{n}{k}\log n = \mathcal O (\frac{n}{\log^2 n} \cdot \log n) = \mathcal O (\frac{n}{\log n}) = o(n)$
		\item[Table LongBlock] LongBlock consists of $\frac{n}{k^2}$ entries, because of one entry for each potential long block. Moreover, it has $k$ columns in order to store the positions of the $k$ 1's, which are inside the block. A table cell requires $\log n$ bits. To sum up, the bit space results in: $|\operatorname{LongBlock}| = \frac{n}{k^2} \cdot k \cdot \log n = \mathcal O (\frac{n}{\log n}) = o(n)$
		\item[Table $N'$] The analysis is similar to $N$ by just using the definition for k': $|N'| = \frac{n}{k'} \cdot \log k^2 = \frac{4n\log\log n}{\log^2\log n} = \mathcal O (\frac{n}{\log\log n}) = o(n)$
		\item[LongMiniBlock] The table consists of $\frac{n}{s}$ potential miniblock entries and for each, with indices for $k'$ 1's, relative to the ending of the previous block. Thus, we get: $|\operatorname{LongMiniBlock}| = \frac{n}{s} \cdot k' \cdot \log k^2 = \frac{n}{\sqrt{k}} \cdot \log^2 k \cdot \log k^2 = \mathcal O(\frac{n\log^3\log n}{\log n}) = o(n)$
		\item[Inblock] Inblock as a lookup table contains $2^s$ different patterns, including $k'$ indices for the position of the i\textsuperscript{th} 1 ($0<i< k'$): $|\operatorname{Inblock}| = 2^s\cdot k' \cdot \log s = \mathcal{O}(\sqrt{n}\cdot \log^3\log n) = o(n)$
	\end{description}	
As can be seen from the analysis, all defined structures require a \emph{bit space} in $o(n)$ and thus, they are succinct.
\begin{ex} An example for the select structures is given in Figure~\ref{fig:11:example_select}. In the upper part, the bit vector $B$ is shown, including the borders for the different block types and indices. The four red-dashed separators indicate the \emph{potential borders} of long blocks. Moreover, there is presented a long miniblock in darker blue and three short miniblocks following. Below, one can see the parameters $k$, $k'$ etc. and the tables \emph{LongBlock}, \emph{LongMiniBlock} and \emph{Inblock}, which are associated by means of corresponding colors with the blocks in the bit-vector.
	\begin{figure}[!ht]
		\centering  
		\includegraphics[width=0.999\textwidth]{images/example_select_query.pdf}
		\caption{Example of select}
		\label{fig:11:example_select}
	\end{figure}
 %For example, the table \emph{LongBlock} contains an entry for index 2. After a lookup in N we know, that second long block starts after index $36$ in B. 
\end{ex}
\subsection{Findclose} 
Again, let $B$ be a balanced string of $2n$ parentheses.
\begin{prob}
	We have to define a succinct data structure that requires $o(n)$ bits of additional space to answer findclose queries in $\mathcal{O}(1)$ time for any balanced string of length $2N \le 2n$. 
\end{prob}
\begin{ex} The following table presents a balanced parentheses string. An example query for findclose would be: $\operatorname{findclose}(1) = 6$.\\
	\begin{tabular}{c c c c c c c c c c c c c c c c c c c c c c c c}
	 &  &   &   &   &   &   &   &   &   &   &   & 1 &   &   &   &   &   &   &   &   &  &  2 & \\
	 &  & 0 & 1 & 2 & 3 & 4 & 5 & 6 & 7 & 8 & 9 & 0 & 1 & 2 & 3 & 4 & 5 & 6 & 7 & 8 & 9 & 0 & 1\\
	$B$ & = & ( & ( & ( & ) & ( & ) & ) & ( & ) & ( & ( & ( & ) & ( & ) & ( & ) & ) & ( & ) & ) & ) \\
	\end{tabular}
\end{ex}\noindent
If $N = \mathcal{O}(\frac{n}{\log^2n})$, we can precompute the answers for all $2N$ parentheses, using $\mathcal{O}(\frac{n}{\log n}) = o(n)$ bits in total. Otherwise, we construct the structures as follows: we divide $B$ into equal-sized blocks of length $s$, where: $s = \lfloor \frac{\log n}{2} \rfloor$.
			\begin{defi} Let $b(p) = \lfloor \frac{p}{s} \rfloor$ denote the block in which parenthesis $p$ lies. Moreover, we let $\mu (p)$ denote the matching parenthesis of $p$: 
				$\mu (p) = \left\{
				  \begin{array}{l l}
				    \operatorname{findclose(p)} & \quad B[p] = \text{'('}\\
				    \operatorname{findopen(p)} & \quad \text{else}\\
				  \end{array} \right.$
			\end{defi}
\begin{defi} 
	Let's call $p$ \emph{far}, if $b(\mu (p)) \ne b(p)$ (the matching parenthesis for $p$ is located in another block) or \emph{near}, if $b(\mu (p)) = b(p)$.
\end{defi}\noindent
\emph{Near Parentheses:}\\
A lookup table $\operatorname{NearFindClose}[0, 2^s-1][0, s-1]$ is precomputed such that: $$\operatorname{NearFindClose}[\text{pattern}][i] = \left\{
  \begin{array}{l l}
    \operatorname{findclose(pattern, i)} &\quad \text{if } pattern[i] \text{ is near} \\
    \bot & \quad \text{if } pattern[i] \text{ is far} \\
  \end{array} \right.$$
$\forall patterns, \forall i: 1\le i\le s$ such that pattern[$i$] = $'('$.
\bigskip\\
\emph{Far Parentheses:}\\
\begin{defi} Consider $p$ as an opening \emph{far} parenthesis and let $q$ be the immediate predecessor of $p$ which is also an opening \emph{far} parenthesis. An opening parenthesis $p$ is called an \emph{opening pioneer} if: $b(\mu (p)) \ne b(\mu (q))$. A \emph{closing pioneer} is defined symmetrically. A \emph{pioneer} is either opening or closing. Note that the match of a pioneer is not necessarily a pioneer itself. The root is always a pioneer.
\end{defi}
\begin{ex}
Figure~\ref{fig:11:ex_pioneer} shows an example of a pioneer $p$ with $s=5$.
	\begin{figure}[!ht]     
		\centering  
		\includegraphics[width=0.5\textwidth]{images/example_pioneer.pdf}
		\caption{Example of a pioneer}  
		\label{fig:11:ex_pioneer}
	\end{figure}
\end{ex}
\noindent The number of pioneers is size of: $\#pioneer = |B'| = \mathcal{O}(\frac{N}{\log n})$, because there can be at most one pair $(p, \mu (p))$ per pair of blocks such that $p$ is in the one block and $\mu (p)$ in the other one, and $p$ or $\mu (p)$ is a pioneer: Imagine a graph, where nodes are represented by blocks and edges represent a pioneer and its match. We can see that the resulting graph is planar, because matching parentheses cannot cross. Hence, its size is linear in the number of blocks, which is $\mathcal{O}(\frac{N}{\log n})$.\bigskip\\
We construct a data structure $B'$, which represents a substring of $B$, but only consisting of pioneers and their matches. To tell whether a parenthesis $p$ is stored in $B'$, the pioneers and their matches are marked in a bitmap $\operatorname{piofam}[0, 2N-1]$ and prepared for $\mathcal{O}(1)$ rank- and select-queries. To keep the space within $o(n)$, we need the following theorem, which will be proved in a further section.
\begin{thm} {Sparse Bitmap Theorem}\\
	A bitmap $B$ of length $N$ containing $u \le N$ 1's can be represented in $\mathcal{O}(u \log \frac{N}{u}) + o(N)$ bits of space, such that subsequent rank- and select-queries on $B$ can be answered in $\mathcal{O}(1)$ time.
\end{thm}
\noindent The same structure is stored recursively for $B'$ such that $ |B''| = \mathcal{O}(\frac{N}{\log^2n})$ with its corresponding bitmap \emph{piofam'}. In this stage, all answers can be precomputed. \\
Additionally, we need two more lookup tables for the final algorithm.
\begin{defi} $\Delta Excess[0, 2^{s-1}][0, s-1][0, s-1]$ represents a lookup table to find differences in excess level, defined as follows.
	$$\Delta Excess[pattern][i][j] = \operatorname{excess}(pattern, j) - \operatorname{excess}(pattern, i)$$
\end{defi}
\begin{defi} $\operatorname{Leftmost}\Delta[pattern][\Delta][i] = \min{\{j\le i : excess(j)-excess(i) = \Delta\}}  $ with $0 \le i < s$
\end{defi}
 \par \noindent
By now, all relevant data structures for the findclose operation are defined. A bit space analysis will verify that the structures are succinct.
\begin{description}
	\item[Vector $B'$] We have already shown that there exist $\#pioneers = |B'| = \mathcal{O}(\frac{N}{\log n})$, which can be stored in $o(n)$ bits.
	\item[Bitmap piofam] Based on the sparse bitmap theorem and for $u = \mathcal{O}(\frac{N}{\log n})$, piofam requires a bit space of: $|\operatorname{piofam}| = \mathcal{O}(\frac{N}{\log n} \cdot \log\log n) + o(N) = o(n)$.
	\item[Vector $B''$] $B''$, which contains all pioneer families of $B'$, requires a bit space of: $|B''| = \mathcal{O}(\frac{N}{\log^2n}) = o(n)$.
	\item[Bitmap piofam'] For the corresponding pioneer bitmap for $B''$, we set $u = \mathcal{O}(\frac{N}{\log^2 n})$, which results in: $|\operatorname{piofam'}| = \mathcal{O}(\frac{N}{\log^2 n} \cdot \log (\log n\cdot \frac{\log^2n}{N})) + o(N) = \mathcal{O}(\frac{N}{\log^2 n} \cdot \log\log n ) + o(N) = o(n)$.
	\item[Table NearFindClose] The lookup table contains an entry for each of the $2^s$ patterns, one entry stores $\mathcal{O}(s)$ indices for near parentheses and each index requires $\log s$ bits. Therefore, a bit space of $\mathcal{O}(2^s \cdot s \cdot \log s) = o(n)$ is required.
	\item[$\Delta$Excess and Leftmost$\Delta$] Similar to NearFindClose, both tables have entries for $2^s$ patterns, $\mathcal{O}(s^2)$ rows and a cell with $\log s$ bits. If the three tables are combined, they require: $\mathcal{O}(2^s \cdot s^2 \cdot \log s) = o(n)$. Because both tables and NearFindClose use the same patterns, it is even possible to precompute a combined lookup table. 
\end{description}
As has been shown for all data structures, each requires a bit space in $o(n)$ and thus, they are still succinct. In the following, we continue with the definition of the algorithm for the operation \emph{findclose(p)}.
\begin{defi} \emph{Operation findclose(p)}
	\begin{enumerate}
		\item Based on the lookup table, determine whether $p$ is far:
		\begin{enumerate}
			\item $p$ is near $\rightarrow$ The table \emph{NearFindClose} gives the answer.
			\item $p$ is far, then calculate the number of members in the pioneer family $B'$ up to $p$ by $ q \leftarrow \operatorname{rank}_1(piofam, p) $ and the position of this parenthesis in $B'$ by $ p^{*} \leftarrow \operatorname{select}_1(piofam, q) $, which is an \emph{opening} parenthesis and the immediate previous pioneer. Using the recursive structure for $B'$, we find that $j \leftarrow \operatorname{findclose}(B', q-1)$\footnote{$q-1$ because rank starts at 1.} is the match of $q$ in $B'$, which can be \emph{mapped back} to a position in $B$ by $\mu (p^{*}) = \operatorname{select}_1(piofam, j+1)$. 
		\end{enumerate}
		\item Since the first far parenthesis in each block is stored in $B'$, $b(p) = b(p^{*})$. Via a table lookup, the excess level difference $\Delta$ between $p^{*}$ and $p$ is determined. Let $b = \lfloor \frac{p}{s} \rfloor$ and set $\Delta \leftarrow \Delta Excess[B[bs, (b+1)s-1]][p-bs][p^{*}-bs]$.
		\item The change between $\mu (p)$ and $\mu (p^{*})$ must be $\Delta$ and $\mu (p)$ is the leftmost position in $\mu (p^{*})$'s block with this property (same excess difference). Thus, we can use the \emph{Leftmost$\Delta$} lookup table, where $b' = \lfloor \frac{\mu (p^{*})}{s} \rfloor$ is $\mu (p^{*})$'s block (hence, also $\mu (p)$'s block) by $$\mu (p) = b'\cdot s + \operatorname{Leftmost}\Delta[B[b'\cdot s, (b' + 1)s -1]][\Delta][\mu (p^{*}) - b' \cdot s]$$
	\end{enumerate}
\end{defi}
\begin{ex}
	Finally, an example of findclose is presented. The required data structures are visible in Figure~\ref{fig:11:ex_findclose}. Separators are indicating block borders for $s=5$. Our example query is: $\operatorname{findclose}(4)$. As we can see from a lookup in \emph{NearFindClose}, the $\bot$ for our $p$ indicates a far parenthesis pair. Next, the algorithm computes the rank$_1$ in \emph{piofam} up to $p$, which results in $q=2$. Based on $q$, the immediate previous pioneer $p^{*}$ is calculated. Moreover, the closing parenthesis for $p^{*}$ is at $j=2$ and $\mu (p^{*}) = 6$ can be determined. The excess difference between $p^{*}$ and $p$ is: $\Delta = 1$ ($\Delta$Excess, last pattern, $i=1$ for index $4$ in pattern). At the end, the $\Delta$Leftmost table is accessed for $\Delta = 1$. In the pattern, $\mu (p^{*})$ is at index $1$ and the table returns a $0$. We can finally calculate $\mu (p) = \lfloor \frac{6}{5} \rfloor \cdot 5 + 0 = 5$.  
		\begin{figure}[!ht]     
			\centering  
			\includegraphics[width=0.7\textwidth]{images/example_findclose.pdf}
			\caption{Example data structures of findclose}  
			\label{fig:11:ex_findclose}
		\end{figure}\\
		\begin{figure}[!ht]     
			\centering  
			\includegraphics[width=0.9\textwidth]{images/example_findclose_table.pdf}
			\caption{Example lookup table of findclose}  
			\label{fig:11:ex_findclose_lookup}
		\end{figure}
\end{ex}

\nocite{*}
\pagebreak
\footnotesize
\bibliographystyle{plainnat}
\label{lib}
\bibliography{library}
\normalsize
\end{document}
